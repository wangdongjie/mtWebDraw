﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using mtTools;
using mtWebDraw.DAL;
using mtWebDraw.Models;
using mtWebDraw.Common;

namespace mtWebDraw.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            string v = Request["v"];
            if (string.IsNullOrWhiteSpace(v))
                return View();
            else
                return View(v);
        }

        public ActionResult Auth()
        {
            var code = Request["authcode"];
            if (code.IsNullOrWhiteSpace())
                return Content("请提供授权码！");

            var Where = NH.CreateWhere("AuthCode", code.Trim());
            IList<tUser> list = NHibernateDal.Select<tUser>(Where);

            if (list.Count > 0) //未停用
            {
                if (list[0].IsStop == false)
                {
                    User user = new User();
                    user.ID = list[0].ID;
                    user.UserName = list[0].UserName;
                    user.FullName = list[0].FullName;
                    user.IsAdmin = list[0].IsAdmin;

                    Session["user"] = user;

                    var chartid = Request["chartid"] ?? "";
                    var dragramid = Request["dragramid"] ?? "";
                    return new RedirectResult("/?chartid=" + chartid + "&dragramid=" + dragramid);
                }
                else
                    return Content("您的帐号已经被停用！");
            }
            else
                return Content("查无无此授权码用户！");
        }
	}
}