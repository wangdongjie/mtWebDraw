﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using mtTools;
using mtWebDraw.DAL;
using mtWebDraw.Models;
using System.Web.Caching;

namespace mtWebDraw.Common
{
    public static class CacheData
    {

        /// <summary>
        /// 统一设置缓存超时时长
        /// </summary>
        public static int cacheTimeOut = 60 * 20;

        /// <summary>
        /// 统一设置超级管理员用户名
        /// </summary>
        public static string sysadmin = ConfigHelper.GetAppSettingValue("SysAdminUserName", "admin");

        #region 缓存唯一Key或前缀

        /// <summary>
        /// 用户登陆时用于指定n次错误则锁定的缓存key
        /// </summary>
        public static readonly string keyLoginLock = "login_username_";

        /// <summary>
        /// 部门信息下拉列表
        /// </summary>
        public static readonly string keySelectDepartmentList = "keySelectDepartmentList";

        /// <summary>
        /// 部门信息列表
        /// </summary>
        public static readonly string keyDepartmentList = "keyDepartmentList";

        /// <summary>
        /// 用户信息下拉列表
        /// </summary>
        public static readonly string keySelectUserList = "keySelectUserList";

        /// <summary>
        /// 用户信息列表
        /// </summary>
        public static readonly string keyUserList = "keyUserList";

        /// <summary>
        /// 单个流程图信息
        /// </summary>
        public static readonly string keyChart = "keyChart_";

        /// <summary>
        /// 单个用户所有流程图权限信息
        /// </summary>
        public static readonly string keyUserChartCodeList = "keyUserChartCodeList_";

        #endregion

        #region 下拉选择信息

        /// <summary>
        /// 获取部门信息下拉列表
        /// </summary>
        /// <returns></returns>
        public static string GetSelectDepartmentList()
        {
            return Cached.Caching<string>(keySelectDepartmentList, () =>
            {
                var list = GetDepartmentList();
                if (list.Count > 0)
                {
                    var objList = new List<object>();
                    foreach (var item in list)
                    {
                        objList.Add(new { ID = item.ID, FullName = item.FullName });
                    }

                    var res = new Success(1, objList, "");
                    return res.JSONSerialize();
                }

                return "";
            }, (s) => { return !s.IsNullOrWhiteSpace(); }, cacheTimeOut, false, new CacheDependency(null, new string[] { keyDepartmentList }));
        }

        /// <summary>
        /// 获取用户信息下拉列表
        /// </summary>
        /// <returns></returns>
        public static string GetSelectUserList(string DepartmentID)
        {
            return Cached.Caching<string>(keySelectUserList + (DepartmentID.IsNullOrWhiteSpace() ? "" : "_" + DepartmentID), () =>
            {
                var list = GetUserList(DepartmentID).Where(p => p.IsStop == false).ToList();
                if(list.Count > 0)
                {
                    var objList = new List<object>();
                    foreach (var item in list)
                    {
                        objList.Add(new { 
                            ID = item.ID, UserName = item.UserName, DepartmentID = item.DepartmentID, FullName = item.FullName, 
                            DisplayName = item.FullName.IsNullOrWhiteSpace() ? item.UserName : item.FullName
                        });
                    }

                    var res = new Success(1, objList, "");
                    return res.JSONSerialize();
                }

                return "";
            }, (s) => { return !s.IsNullOrWhiteSpace(); }, cacheTimeOut, false, new CacheDependency(null, new string[] { keyUserList }));
        }

        #endregion

        #region 部门信息

        /// <summary>
        /// 获取部门信息列表
        /// </summary>
        /// <returns></returns>
        public static IList<tDepartment> GetDepartmentList()
        {
            return Cached.Caching<IList<tDepartment>>(keyDepartmentList, () =>
            {
                return NHibernateDal.Select<tDepartment>(NH.CreateOrderBy("FullName", OrderByType.Asc));
            }, (list) => { return list.Count > 0; }, cacheTimeOut);
        }

        /// <summary>
        /// 清空部门信息缓存
        /// </summary>
        public static void ClearDepartmentList()
        {
            CacheHelper.RemoveCache(keyDepartmentList);
        }

        #endregion

        #region 用户信息

        /// <summary>
        /// 获取用户信息列表
        /// </summary>
        /// <returns></returns>
        public static IList<tUser> GetUserList(string DepartmentID)
        {
            IList<tUser> data = Cached.Caching<IList<tUser>>(keyUserList, () =>
            {
                var list = NHibernateDal.Select<tUser>(NH.CreateOrderBy("FullName", OrderByType.Asc));
                foreach (var item in list)
                {
                    item.Password = "";
                }
                return list;
            }, (list) => { return list.Count > 0; }, cacheTimeOut);

            return DepartmentID.IsGuid() ? data.Where(p => p.DepartmentID == DepartmentID.ToGuid()).ToList() : data;
        }

        /// <summary>
        /// 清空用户信息缓存
        /// </summary>
        public static void ClearUserList()
        {
            CacheHelper.RemoveCache(keyUserList);
        }

        #endregion

        #region 单个流程图信息缓存

        /// <summary>
        /// 获取单个流程图信息
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static tChart GetChart(Guid ID)
        {
            return Cached.Caching<tChart>(keyChart + ID.ToString().ToLower(), () =>
            {
                return NHibernateDal.Select<tChart>(ID);
            }, (obj) => { return obj != null; }, cacheTimeOut);
        }

        /// <summary>
        /// 清空单个流程图信息缓存
        /// </summary>
        /// <param name="ID"></param>
        public static void ClearChart(Guid ID)
        {
            CacheHelper.RemoveCache(keyChart + ID.ToString().ToLower());
        }

        #endregion

        #region 单个用户所有流程图权限信息缓存

        /// <summary>
        /// 获取单个用户所有流程图权限信息
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static IList<tUserChart> GetUserChartCodeList(Guid ID)
        {
            return Cached.Caching<IList<tUserChart>>(keyUserChartCodeList + ID.ToString().ToLower(), () =>
            {
                return NHibernateDal.Select<tUserChart>(NH.CreateWhere("UserID", ID));
            }, (list) => { return list.Count > 0; }, cacheTimeOut);
        }

        /// <summary>
        /// 清空单个用户所有流程图权限信息缓存
        /// </summary>
        /// <param name="ID"></param>
        public static void ClearUserChartCodeList(Guid ID)
        {
            CacheHelper.RemoveCache(keyUserChartCodeList + ID.ToString().ToLower());
        }

        #endregion

    }
}